import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NzTypographyModule } from 'ng-zorro-antd/typography';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzDividerModule } from 'ng-zorro-antd/divider';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    NzTypographyModule,
    NzInputModule,
    NzGridModule,
    NzTableModule,
    NzDividerModule
  ],
  exports: [
    NzTypographyModule,
    NzInputModule,
    NzGridModule,
    NzTableModule,
    NzDividerModule
  ]
})
export class NgZoroModule { }
