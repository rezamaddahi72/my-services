import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import {HttpClientModule} from "@angular/common/http";
import {NgZoroModule} from "./ng-zoro/ng-zoro.module";
import {RouterModule} from "@angular/router";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
// import { PersianDateModule } from 'angular2-persian-date';


@NgModule({
  declarations: [
    HeaderComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    NgZoroModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,

  ],
  exports: [
    HeaderComponent,
    NgZoroModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,

  ]
})
export class SharedModule { }
