import { Component } from '@angular/core';
import {SharedService} from "./shared/shared.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  constructor(private sharedService: SharedService) {
  }

  onResize(event:any) {
    this.sharedService.pageHeight.next(event.target.innerHeight);
  }
}
