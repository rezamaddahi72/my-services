import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MyServicesRoutingModule } from './my-services-routing.module';
import { MyServicesComponent } from './my-services/my-services.component';
import {SharedModule} from "../shared/shared.module";
import { SearchBoxComponent } from './search-box/search-box.component';
import { ServiceCategoriesComponent } from './service-categories/service-categories.component';


@NgModule({
  declarations: [
    MyServicesComponent,
    SearchBoxComponent,
    ServiceCategoriesComponent,
  ],
  imports: [
    CommonModule,
    MyServicesRoutingModule,
    SharedModule,
  ]
})
export class MyServicesModule { }
