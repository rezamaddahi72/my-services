import { Component, OnInit } from '@angular/core';
import {ServicesModel} from "../models/services.model";
import {SharedService} from "../../shared/shared.service";

@Component({
  selector: 'app-my-services',
  templateUrl: './my-services.component.html',
  styleUrls: ['./my-services.component.scss']
})
export class MyServicesComponent implements OnInit {

  servicesList: ServicesModel[] = [];
  filterTerm = '';
  constructor(private sharedService: SharedService) {
    this.sharedService.getServices();
    this.sharedService.servicesList.subscribe(services => {
      this.servicesList = services;
    });
  }

  ngOnInit(): void {
  }

  onChangeServiceType(serviceType: string) {
    this.sharedService.servicesList.subscribe(services => {
      this.servicesList = services;
    });
    this.filterTerm = serviceType;
    let services = [];
    switch (serviceType) {
      case 'all-services':
        break;
      case 'hosting':
        for (const service of this.servicesList) {
          if ((service.type === 'Hosting')) {
            services.push(service);
          }
        }
        this.servicesList = services;
        break;
      case 'cdn':
        for (const service of this.servicesList) {
          if ((service.type === 'CDN')) {
            services.push(service);
          }
        }
        this.servicesList = services;
        break;
      case 'domain':
        for (const service of this.servicesList) {
          if ((service.type === 'Domain')) {
            services.push(service);
          }
        }
        this.servicesList = services;
        break;
      case 'server':
        for (const service of this.servicesList) {
          if ((service.type === 'Server')) {
            services.push(service);
          }
        }
        this.servicesList = services;
        break;
      case 'other':
        for (const service of this.servicesList) {
          if ((service.type === 'Other')) {
            services.push(service);
          }
        }
        this.servicesList = services;
        break;
      case 'ssl':
        for (const service of this.servicesList) {
          if ((service.type === 'SSl')) {
            services.push(service);
          }
        }
        this.servicesList = services;
        break;
    }
  }

  onSearchInput(searchTerm: string) {
    this.sharedService.servicesList.subscribe(services => {
      const serviceList = [];
      for (const service of services) {
        if (service.type.toLowerCase() === this.filterTerm) {
          serviceList.push(service);
        }
      }
      this.servicesList = serviceList;
    });
    if (searchTerm !== '') {
      const services = [];
      for (const service of this.servicesList) {
        if ((service.name.trim().toLowerCase()).includes(searchTerm.trim().toLowerCase())) {
          services.push(service);
        }
      }
      this.servicesList = services;
    } else {
      if (this.filterTerm === '' || this.filterTerm === 'all-service') {
        this.sharedService.servicesList.subscribe(services => {
          this.servicesList = services;
        });
      } else {
        this.sharedService.servicesList.subscribe(services => {
          const serviceList = [];
          for (const service of services) {
            if (service.type.toLowerCase() === this.filterTerm) {
              serviceList.push(service);
            }
          }
          this.servicesList = serviceList;
        });
      }
    }
  }

}
