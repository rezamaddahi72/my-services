export interface ServicesModel {
  name: string;
  type: string;
  id: string;
  status: string;
  nextduedate: string;
  amount: string;
  invoiceUrl: string;
}
