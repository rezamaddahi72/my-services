import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";

@Component({
  selector: 'app-search-box',
  templateUrl: './search-box.component.html',
  styleUrls: ['./search-box.component.scss']
})
export class SearchBoxComponent implements OnInit {

  @Output() term = new EventEmitter<string>();

  myForm = new FormGroup({
    searchTerm: new FormControl('')
  });

  constructor() { }

  ngOnInit(): void {
  }

  onSubmitSearch(form:any) {
    this.term.emit(form.value.searchTerm);
  }

}
