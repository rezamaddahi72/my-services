import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {ServicesModel} from "../models/services.model";
import {SharedService} from "../../shared/shared.service";

@Component({
  selector: 'app-service-categories',
  templateUrl: './service-categories.component.html',
  styleUrls: ['./service-categories.component.scss']
})
export class ServiceCategoriesComponent implements OnInit {
  servicesList: ServicesModel[] = [];
  cdnServices: ServicesModel[] = [];
  hostingServices: ServicesModel[] = [];
  sslServices: ServicesModel[] = [];
  serverServices: ServicesModel[] = [];
  domainServices: ServicesModel[] = [];
  otherServices: ServicesModel[] = [];
  filterTerm = 'all-services';
  innerHeight: number = 0;
  @Output() serviceFilter = new EventEmitter<string>();
  constructor(private sharedService: SharedService) {
    this.sharedService.servicesList.subscribe(services => {
      this.servicesList = services;
    });
    this.sharedService.pageHeight.subscribe(height => {
      this.innerHeight = height;
    });
    for (const service of this.servicesList) {
      switch (service.type) {
        case 'CDN':
          this.cdnServices.push(service);
          break;
        case 'Hosting':
          this.hostingServices.push(service);
          break;
        case 'SSl':
          this.sslServices.push(service);
          break;
        case 'Server':
          this.serverServices.push(service);
          break;
        case 'Domain':
          this.domainServices.push(service);
          break;
        case 'Other':
          this.otherServices.push(service);
          break;
      }
    }
  }

  ngOnInit(): void {
  }

  onFilterServices(serviceFilter: string) {
      this.serviceFilter.emit(serviceFilter);
      this.filterTerm = serviceFilter;
  }

}
